'''
@file       Homework_01.py

@brief      This file calculates change based on a price and payment.

@details    Price is inputted as an integer and the payment is inputted
            as a tuple. The tuple is a list of the currency inputted from 
            pennies up to twenty dollar bills. If the payment is greater than
            the price, the function returns the change as a tuple. If the 
            payment is less than the price, the function indicates 
            insufficient funds.

'''

def getChange(price , payment):
    
    import math
    
    ## Defines cost variable as price input parameter
    cost = price*100
    
    ## Defines fund as zero prior to c
    fund = 0
    fund += payment[0]
    fund += payment[1]*5
    fund += payment[2]*10
    fund += payment[3]*25
    fund += payment[4]*100
    fund += payment[5]*100*5
    fund += payment[6]*100*10
    fund += payment[7]*100*20
        
    if(fund >= cost):
        
        ## Defines change as the difference between price and payment
        change = fund - cost
        
        ## Variable defining number of twenties to return
        twenty = math.floor(change/(20*100))
        change -= twenty*20*100
        
        ## Variable defining number of tens to return
        ten = math.floor(change/(10*100))
        change -= ten*10*100
        
        ## Variable defining number of fives to return
        five = math.floor(change/(5*100))
        change -= five*5*100
        
        ## Variable defining number of ones to return
        one = math.floor(change/(100))
        change -= one*100
        
        ## Variable defining number of quarters to return
        quarter = math.floor(change/(25))
        change -= quarter*25
        
        ## Variable defining number of dimes to return
        dime = math.floor(change/(10))
        change -= dime*10
        
        ## Variable defining number of nickels to return
        nickel = math.floor(change/(5))
        change -= nickel*5
        
        ## Variable defining number of pennies to return
        penny = math.floor(change)
        
        return((penny , nickel , dime , quarter , one , five , ten , twenty))
    
    else:
        print('Insufficient Funds')
        pass