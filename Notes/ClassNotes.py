'''
@file       ClassNotes.py

@brief      This file contains class notes from 1/15/21.
'''

class food:
    '''
    Dont forget your docstring
    '''
    
    # These definitions are static (belong to the class, not object)
    
    ## Calories per gram of fat
    fatCal = 9
    proCal = 4
    carbCal = 4
    
    def __init__(self, name, macros, temperature = 0):
        '''
        @brief  Creates a food object
        @param name The name of the food
        @param macros A list containing the quantity (in grams) of each
                    macronutrient, [fat, protein, carbs]
        @param temperature The temp of the food item in degs F
        '''
        
        ## The name of the food
        self.foodName = name
        
        ## The temp of the food
        self.temperature = temperature # saves input parameter as class attribute
        
        self.calories = self.fatCal*macros[0] + self.proCal*macros[1] + self.carCal*macros[3]
        
    def cook(self, temp, time):
        self.temperature = temp
        
        return "Ding, food is done!"
        
if __name__ == "__main__":
    myFood = food('Sandwich', temperature = 75)
    yourFood = food('Pizza', macros = [20, 5, 15])
    