'''
@file       GeneratorEx.py

@brief      

@details    
'''

import time

# Fibonacci function
def fibNum(idx):
    # Seed 0
    if idx == 0:
        return 0
    
    # Seed 1
    elif idx == 1:
        return 1
    # Any other index
    else:
        f0 = 0
        f1 = 1
        
        for n in range(2,idx+1):
            f2 = f0 + f1
            f0 = f1
            f1 = f2
        return f2

# Fibonacci generator
def fibSequence(numIterations):
    # Seed 0
    f0 = 0
    yield f0
    
    # Seed 1
    f1 = 1
    yield f1
    
    counter = 2
    
    # Any other index
    while (counter < numIterations):
        
         f2 = f0 + f1
         f0 = f1
         f1 = f2
         
         counter += 1
         
         yield f2

# Try calling the generator with next()
mySequence = fibSequence(10)

# Run first iteration of mySequence
print(next(mySequence))
# Run second iteration of mySequence
print(next(mySequence))
# Run third iteration of mySequence
print(next(mySequence))

# Try calling the generator repeatedly with for()
for fibNumber in fibSequence(10):
    print(fibNumber)
    time.sleep(0.1)

myList = list(fibSequence(10))
print(myList)



