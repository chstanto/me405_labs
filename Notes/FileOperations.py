'''
@file       FileOperations.py

@brief      

@details    
'''

from matplotlib import pyplot

col1 = list()
col2 = list()
col3 = list()

# open the file, for lab 03 - 'with serial(args) as serport:'
with open('data.csv') as file:

    # file = open('data.csv') # to open a file which creates a variable
    # file.readline() reads a single line from the file
    # running a second time will return the second line of the file
    # returned line includes multiple numbers and some 'junk'
    
    # Read one line of the file
    # myRow = file.readline()
    
    # Read all lines of the file
    myRows = file.readlines()
    
    # loop through each row from csv
    for myRow in myRows:
    
        # want to get rid of \n at end of readline
        # myRow.strip('\n')
        # want to strip and split numbers apart
        myList = myRow.strip('\n').split(',')
        # myRow is now a list of three strings ie '0.004'
        # want to convert list to floats which can actually be used
        # float(myList[0])
        
        # Append converted value from columns to column lists
        col1.append(float(myList[0]))
        col2.append(float(myList[1]))
        col3.append(float(myList[2]))

# Dont forget to close the file before running
# file.close()

# create new figure window
pyplot.figure()

# Create subplots for column data
pyplot.subplot(2,1,1)
pyplot.plot(col1,col2)
pyplot.ylabel('Column 2 Data')
pyplot.xlabel('Column 1 Data')

pyplot.subplot(2,1,2)
pyplot.plot(col1,col3)
pyplot.ylabel('Column 3 Data')
pyplot.xlabel('Column 1 Data')






















